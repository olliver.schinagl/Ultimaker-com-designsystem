export default {
    heroProduct: require('../../organisms/hero-product/hero-product.stories.json'),
    ubr: require('../../organisms/unique-buying-reasons/ubr.stories.json'),
    examples: require('../../organisms/examples/examples.stories.json'),
    colors: require('../../organisms/colors/colors.stories.json')
};
