# Reseller card

A *'Reseller card'* groups a company logo, name and amount of showrooms.

The card is clickable and directs the user to the Ultimaker overview page at the company's website.
Therefore a correct URL needs to be provided.

## Logo guidelines

The resolution of the logo should be in the highest dimensions available.

### Sizes logo:
- 120 px × 120 px *(smallest to largest screens + retina)*

### File types:
- (transparent) PNG
- SVG

## Patterns
A *'Reseller card'* is used within the *'[Resellers](/?selectedKind=Molecules%7Clayout%2Fcards&selectedStory=Reseller%20card&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fnotes%2Fpanel)'* organism.
