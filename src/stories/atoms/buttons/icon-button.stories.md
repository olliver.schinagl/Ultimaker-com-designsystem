# Icon button

By default an *'icon button'* is as large as the other buttons.
We use 2 kinds of icon buttons that look the same.
- If an *'icon button'* acts like a toggle we use the *Icon Button*
- If an *'icon button'* leads to another part of the page or another page we use the *Icon Anchor Button*.

There's also a *'large'* and a *'small'* version for special cases. E.g. in the footer we used a smaller icon button for social icons.
A visual label in the icon button is optional. Although the label is overwritten by the aria-label.
