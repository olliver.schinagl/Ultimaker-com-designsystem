# Buttons

We use 3 kinds of buttons that look the same.
- If a *'button'* acts like a toggle we use the *Button*,
- If a *'button'* leads to another part of the page or another page we use the *Anchor Button*
- If we want to submit a form we use *Input button* or *Input submit button*.

There are 3 different appearances, default, primary and secondary.
- The default button can be used multiple times on a page standalone or combined with a secondary button.
- The primary button only once on a page as a standalone cta.
- The secondary button can be used multiple times on a page standalone or combined with a default button.

The input and input submit button will always have the default appearance.
