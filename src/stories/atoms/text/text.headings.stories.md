# Headings

We use 6 levels of headlines, each serving a semantical purpose.

- *'H1'* as a page title
- *'H2'* as a title for each organism
- *'H3'* as a title for each molecule within an organism
- *'H4'* as a title within a paragraph of normal copy
- *'H5'* as a title within a paragraph of small copy (e.g. image captions)
- *'H6'* as a title with the smallest appearance on the deepest level (e.g. legal in footer)

## Font usage
*'H1' - 'H3'* are styled with the secondary font and used as
larger headings outside of paragraphs.

*'H4' - 'H6'* are styled with the primary font and used inside of paragraphs and smaller
text sections like small copy.
