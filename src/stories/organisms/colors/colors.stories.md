# Colors

The *'Colors'* organism displays all colors available for a material or material family. It also has a call-to-action to sell the product.
This organism should at least contain a title, color, image per color and a link to the resellers.

**For the tooltip JavaScript  is used to check if it's within the viewport boundaries, and adds a class to push the
tooltip to the left or the right to keep the tooltip in the viewport.**


**For the icons on the color swatches JavaScript  is used to check the background color to see if the 'show' icon needs
to be black or white for enough contrast.**

## Photography guidelines

Photography of the per material image should be in the highest dimensions per breakpoint.

### Sizes full width image:
- 900 px × 540 px *(small screens)*
- 1800 px × 1080 px *(large screens)*
