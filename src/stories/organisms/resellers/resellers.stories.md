# Resellers

The *'Resellers'* organism divides preferred and authorized resellers into their own categories to be able to highlight the preferred resellers.

## Types
In order to direct the user to a reseller that can deliver most value, the resellers are divided into 2 types:
- Preferred
- Authorized

### Preferred resellers
Preferred resellers are the resellers that add most value for the user and are therefore highlighted in the 1st section of the page.
We always show all preferrred resellers as their value should be equal to other preferred resellers.

#### Tooltip
The tooltip should give the user more info about what distinguishes the preferred from the authorized resellers.
Long copy should be avoided,

### Authorized resellers
Authorized resellers are the default resellers that will be displayed in the 2nd section.
There's a max. amount of authorized resellers that will be available by default.
A button is added below the section to show more resellers.

## Labels
Labels like e.g. *'More info'*, *'Visit website'*, *'showroom'* and *'Show all'* should be kept as short but as clear as possible.

## Patterns
Each reseller is displayed in a [Reseller card](/?selectedKind=Molecules%7Clayout%2Fcards&selectedStory=Reseller%20card&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fnotes%2Fpanel).
The *'Resellers'* organism is used within the *'Resellers'* template
