export default {
    name: 'cta-block',
    template: require('./cta-block.html'),
    props: {
        ctas: {
            type: Object
        }
    }
};
