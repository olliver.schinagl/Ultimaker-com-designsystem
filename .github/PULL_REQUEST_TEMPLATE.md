#### Description
JIRA: [UWEB-1234](https://jira.ultimaker.com:8443/browse/UWEB-1234)

_Change the JIRA ticket number above, and describe the change in this Pull Request._

#### Branches
Fill in the correct branches to use for integration testing in a Playground.

Ultimaker.com-frontend: `master`

Ultimaker.com-craftcms: `master`

Ultimaker.com-geoip: `master`

Ultimaker.com-designsystem: `master`

#### Playground
To spin up a Playground, fill in your name, and the above branches in [Jenkins](https://jenkins.svc.k8s.ultimaker.com/job/Jobs/job/deploy-playground/build?delay=0sec).
